using System;
using System.Collections.Generic;
using Abilities;
using Managers;
using UnityEngine;

namespace SkillsTree
{
    public class SkillTree : MonoBehaviour
    {
        private int _curSkillPoints;
        public int CurSkillPoints => _curSkillPoints;
        
        private List<Ability> _learntAbilities = new List<Ability>();


        private void OnEnable()
        {
            SkillTreeEvents.OnAbilityLearnt.AddListener(OnAbilityLearnt);
            SkillTreeEvents.OnAbilityUnlearnt.AddListener(OnAbilityUnlearnt);
        }

        private void OnDisable()
        {
            SkillTreeEvents.OnAbilityLearnt.RemoveListener(OnAbilityLearnt);
            SkillTreeEvents.OnAbilityUnlearnt.RemoveListener(OnAbilityUnlearnt);
        }

        private void OnAbilityLearnt(Ability ability)
        {
            if (!_learntAbilities.Contains(ability)) {
                _learntAbilities.Add(ability);
            }
            
            _curSkillPoints -= ability.Cost;
            SkillTreeEvents.InvokeSkillPointAmountChanged(_curSkillPoints);
        }
        
        private void OnAbilityUnlearnt(Ability ability)
        {
            if (ability.CurLvl <= 0) {
                _learntAbilities.Remove(ability);
            }
            
            _curSkillPoints += ability.Cost;
            SkillTreeEvents.InvokeSkillPointAmountChanged(_curSkillPoints);
        }
        
        public void UnlearnAllAbilities()
        {
            foreach (var ability in _learntAbilities)
            {
                _curSkillPoints += ability.CurLvl * ability.Cost;
                SkillTreeEvents.InvokeAbilityFullUnlearn(ability);
            }
            
            SkillTreeEvents.InvokeSkillPointAmountChanged(_curSkillPoints);
        }

        public void AddSkillPoint()
        {
            _curSkillPoints++;
            SkillTreeEvents.InvokeSkillPointAmountChanged(_curSkillPoints);
        }

    }
}
