using Abilities;
using UnityEngine;
using UnityEngine.Events;

namespace Managers
{
    public static class SkillTreeEvents
    {
        public static UnityEvent<Ability, bool, bool> OnAbilitySelected = new UnityEvent<Ability, bool, bool>();
        public static void InvokeAbilitySelected(Ability ability, bool connectedLearnt, bool canBeUnlearnt)
        {
            OnAbilitySelected.Invoke(ability, connectedLearnt, canBeUnlearnt);
        }
        
        public static UnityEvent<Ability> OnAbilityLearnt = new UnityEvent<Ability>();
        public static void InvokeAbilityLearnt(Ability ability)
        {
            OnAbilityLearnt.Invoke(ability);
        }

        public static UnityEvent<Ability> OnAbilityUnlearnt = new UnityEvent<Ability>();
        public static void InvokeAbilityUnlearnt(Ability ability)
        {
            OnAbilityUnlearnt.Invoke(ability);
        } 
        
        public static UnityEvent<Ability> OnAbilityFullUnlearn = new UnityEvent<Ability>();
        public static void InvokeAbilityFullUnlearn(Ability ability)
        {
            OnAbilityFullUnlearn.Invoke(ability);
        }
        
        public static UnityEvent<int> OnSkillPointAmountChanged = new UnityEvent<int>();
        public static void InvokeSkillPointAmountChanged(int newValue)
        {
            OnSkillPointAmountChanged.Invoke(newValue);
        }
    }
}
