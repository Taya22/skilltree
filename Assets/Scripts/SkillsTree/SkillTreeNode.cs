using System;
using System.Linq;
using Abilities;
using Managers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SkillsTree
{
    public class SkillTreeNode : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] private Image _abilityImage;
        [SerializeField] private Ability _ability;
        [SerializeField] private SkillTreeNode[] _prevSkills;
        [SerializeField] private SkillTreeNode[] _nextSkills;
        [SerializeField] private bool _fromBase;

        private void OnEnable()
        {
            SkillTreeEvents.OnAbilityLearnt.AddListener(UpdateVisuals);
            SkillTreeEvents.OnAbilityUnlearnt.AddListener(UpdateVisuals);
            SkillTreeEvents.OnAbilityFullUnlearn.AddListener(UpdateVisuals);
        }

        private void OnDisable()
        {
            SkillTreeEvents.OnAbilityLearnt.RemoveListener(UpdateVisuals);
            SkillTreeEvents.OnAbilityUnlearnt.RemoveListener(UpdateVisuals);
            SkillTreeEvents.OnAbilityFullUnlearn.RemoveListener(UpdateVisuals);
        }
        
        private void Start()
        {
            _abilityImage.sprite = _ability.Icon;
            UpdateVisuals(_ability);
        }
        
        private void UpdateVisuals(Ability ability)
        {
            if (ability != _ability) return;
            
            _abilityImage.color = AbilityLearnt() ? Color.white : Color.gray;
        }
        
        public void OnPointerDown(PointerEventData eventData)
        {
            var canBeUnlearned = true;
            foreach (var stb in _nextSkills)
            {
                if(!stb.AbilityLearnt()) continue;
                canBeUnlearned = stb.CheckIfPrevLearnt(this);
                if(!canBeUnlearned) break;
            }
            
            SkillTreeEvents.InvokeAbilitySelected(_ability, CheckIfPrevLearnt(), canBeUnlearned);
        }

        private bool CheckIfPrevLearnt(SkillTreeNode skillToIgnore = null)
        {
            return _fromBase || _prevSkills.Where(stb => stb != skillToIgnore).Any(stb => stb.AbilityLearnt());
        }
        public bool AbilityLearnt() => _ability.CurLvl > 0;
    }
}