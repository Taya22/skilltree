using System;
using Abilities;
using Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SkillsTree
{
    public class SkillTreeInfo : MonoBehaviour
    {
        [SerializeField] private SkillTree _skillTree;
        [SerializeField] private Image _iconImage;
        [SerializeField] private TMP_Text _nameText;
        [SerializeField] private TMP_Text _costText;
        [SerializeField] private TMP_Text _descriptionText;
        [SerializeField] private TMP_Text _levelText;
        [SerializeField] private GameObject _notEnoughPoints;
        [SerializeField] private Slider _levelSlider;
        [SerializeField] private Button _learnBtn;
        [SerializeField] private Button _unLearnBtn;
        private Ability _selectedAbility;
        
        private void OnEnable()
        {
            SkillTreeEvents.OnAbilitySelected.AddListener(UpdateAbilityInfo);
            SkillTreeEvents.OnAbilityFullUnlearn.AddListener(OnFullUnlearn);
        }

        private void OnDisable()
        {
            SkillTreeEvents.OnAbilitySelected.RemoveListener(UpdateAbilityInfo);
            SkillTreeEvents.OnAbilityFullUnlearn.RemoveListener(OnFullUnlearn);
        }

        private void Start()
        {
          Reset();
        }

        private void Reset()
        {
            _nameText.text = "Select Ability";
            _descriptionText.text = "";
            _costText.text = "";
            _iconImage.sprite = null;
            _levelText.text = "0/0";
            _levelSlider.value = 0;
            _learnBtn.gameObject.SetActive(false);
            _unLearnBtn.gameObject.SetActive(false);
            _notEnoughPoints.SetActive(false);
        }
        
        private void OnFullUnlearn(Ability ability)
        {
            Reset();
        }

        private void UpdateAbilityInfo(Ability newAbility, bool connectedLearnt, bool canBeUnlearnt)
        {
            _selectedAbility = newAbility;
            UpdateButtons(connectedLearnt, canBeUnlearnt);
            UpdateTexts();
            UpdateSlider();
            IsMaxLevel(_selectedAbility);
        }
        
        private void UpdateTexts()
        {
            _iconImage.sprite = _selectedAbility.Icon;
            _nameText.text = _selectedAbility.Name;
            _descriptionText.text = _selectedAbility.Description; 
            _costText.text = $"Cost {_selectedAbility.Cost} SP";
        }

        private void UpdateButtons(bool connectedLearnt, bool canBeUnlearnt)
        {
            _notEnoughPoints.SetActive(false);
            
            var canLearn = connectedLearnt &&
                           !IsMaxLevel(_selectedAbility);

            if (canLearn && _selectedAbility.Cost > _skillTree.CurSkillPoints)
            {
                canLearn = false;
                _notEnoughPoints.SetActive(true);
            }

            _learnBtn.gameObject.SetActive(canLearn);
            _unLearnBtn.gameObject.SetActive(_selectedAbility.CurLvl >= 1 && canBeUnlearnt);
        }

        public void LearnAbility()
        {
            if(IsMaxLevel(_selectedAbility) || _selectedAbility == null) return;
            SkillTreeEvents.InvokeAbilityLearnt(_selectedAbility);
            UpdateSlider();
            UpdateButtons(true, true);
        }

        public void UnlearnAbility()
        {
            if(_selectedAbility.CurLvl <= 0 || _selectedAbility == null) return;
            SkillTreeEvents.InvokeAbilityUnlearnt(_selectedAbility);
            UpdateSlider();
            UpdateButtons(true, true);
        }

        private void UpdateSlider()
        {
            _levelText.text = $"{_selectedAbility.CurLvl}/{_selectedAbility.MaxLvl}";
            _levelSlider.maxValue = _selectedAbility.MaxLvl;
            _levelSlider.value = _selectedAbility.CurLvl;
        }
        
        private bool IsMaxLevel(Ability ability)
        {
            return ability.CurLvl >= ability.MaxLvl;
        }
    }
}
