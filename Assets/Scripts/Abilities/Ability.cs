using System;
using Character;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Abilities
{
    [CreateAssetMenu(menuName = "Abilities/New Ability", fileName = "New Ability")]
    public class Ability : ScriptableObject
    {
        [Header("Visuals")]
        public Sprite Icon;
        public string Name;
        [TextArea]
        public string Description;
        public int Cost;
        
        [Header("Stats")]
        [SerializeField] private AbilityType _abilityType;
        public AbilityType AbilityType => _abilityType;
        [SerializeField] private float _amountToAdd;
        [SerializeField] private int _maxLvl;
        [SerializeField] private int _curLvl;

        public int MaxLvl => _maxLvl;
        public int CurLvl => _curLvl;

        //used to reset SOs in editor
        private void OnEnable()
        {
            _curLvl = 0;
        }

        public float OnLevelUp(float curValue)
        {
            if (_curLvl >= _maxLvl) return curValue;
            
            _curLvl++;
            return curValue + _amountToAdd;
        }

        public float OnLevelDown(float curValue)
        {
            if (_curLvl <= 0) return curValue;
            
            _curLvl--;
            return curValue - _amountToAdd;
        }

        public float ForceFullUnlearn(float curValue)
        {
            _curLvl = 0;
            return curValue - _curLvl * _amountToAdd;
        }
    }
}
