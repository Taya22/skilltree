using System;
using Character;
using Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class GameCanvas : MonoBehaviour
    {
        [SerializeField] private Slider _hpSlider;
        [SerializeField] private TMP_Text _hpText;
        [SerializeField] private Slider _energySlider;
        [SerializeField] private TMP_Text _energyText;
        [SerializeField] private TMP_Text _skillPointText;

        private void OnEnable()
        {
            CharacterStats.OnHpValueChange.AddListener(OnHpValueChange);
            CharacterStats.OnEnergyValueChange.AddListener(OnEnergyValueChange);
            SkillTreeEvents.OnSkillPointAmountChanged.AddListener(OnSkillPointAmountChanged);
        }

        private void OnDisable()
        {
            CharacterStats.OnHpValueChange.RemoveListener(OnHpValueChange);
            CharacterStats.OnEnergyValueChange.RemoveListener(OnEnergyValueChange);
            SkillTreeEvents.OnSkillPointAmountChanged.RemoveListener(OnSkillPointAmountChanged);
        }

        private void OnSkillPointAmountChanged(int newValue)
        {
            _skillPointText.text = $"{newValue} SP";
        }

        private void OnHpValueChange(float newValue, float maxValue)
        {
            _hpSlider.maxValue = maxValue;
            _hpSlider.value = newValue;
            _hpText.text = $"{(int)newValue}/{(int)maxValue}";
        }
    
        private void OnEnergyValueChange(float newValue, float maxValue)
        {
            _energySlider.maxValue = maxValue;
            _energySlider.value = newValue;
            _energyText.text = $"{(int)newValue}/{(int)maxValue}";
        }
    }
}
