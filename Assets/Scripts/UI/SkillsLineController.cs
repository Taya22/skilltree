using System;
using UnityEngine;

namespace UI
{
    public class SkillsLineController : MonoBehaviour
    {
        [SerializeField] private LineRenderer _lr;
        [SerializeField] private RectTransform[] _points;
        
        private void Update()
        {
            _lr.positionCount = _points.Length;
            for (var i = 0; i < _points.Length; i++)
            {
                _lr.SetPosition(i, _points[i].position);
            }
        }
    }
}
