using System;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

namespace Character
{
    public class PlayableCharacter : MonoBehaviour
    {
        [SerializeField] private CharacterStats _stats;
        [SerializeField] private Rigidbody2D _rb;
        [SerializeField] private Collider2D _col;
        [SerializeField] private LayerMask _groundLayer;
        private Vector2 _moveDir;

        private bool _canJumpAgain;
        
        private void FixedUpdate()
        {
            _rb.velocity = new Vector2(_moveDir.x * (_stats.GetCurStats[AbilityType.Speed]), _rb.velocity.y);

            if (IsGrounded()) _canJumpAgain = true;
        }

        public void OnMoveLeft(bool isPressed)
        {
            _moveDir = new Vector2(isPressed ? -1 : 0, _moveDir.y);
        }
        
        public void OnMoveRight(bool isPressed)
        {
            _moveDir = new Vector2(isPressed ? 1 : 0, _moveDir.y);
        }

        public void Jump()
        {
            if(!_stats.CanJump) return;
            
            if (!IsGrounded() && _stats.CanDoubleJump)
            {
                if (!_canJumpAgain) return;
                _canJumpAgain = false;
            }
            
            _rb.velocity = new Vector2(_rb.velocity.x, _stats.GetCurStats[AbilityType.JumpHeight]);
        }
        
        private bool IsGrounded()
        {
            return Physics2D.BoxCast(_col.bounds.center, _col.bounds.size, 0f, Vector2.down, 0.1f, _groundLayer);
        }
    }
}
