namespace Character
{
    public enum AbilityType
    {
        MaxHp,
        HpRegen,
        Speed,
        JumpHeight,
        MaxEnergy,
        Damage,
        AttackSpeed,
        Jump,
        DoubleJump,
        DoubleShoot
    }
}