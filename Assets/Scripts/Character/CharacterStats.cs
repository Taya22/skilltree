using System;
using System.Collections.Generic;
using Abilities;
using Managers;
using UnityEngine;
using UnityEngine.Events;

namespace Character
{
    public class CharacterStats : MonoBehaviour
    {
        [Header("Init Stats")]
        [SerializeField] private float _maxHp = 10;
        [SerializeField] private float _hpRegen = 0.1f;
        [SerializeField] private float _moveSpeed = 1f;
        [SerializeField] private float _jumpHeight = 1f;
        [SerializeField] private float _energy = 5f;
        [SerializeField] private float _damage = 1f;
        [SerializeField] private float _attackSpeed = 1f;
        private float _curHp;
        private float _curEnergy;

        private Dictionary<AbilityType, float> _curStatsDict;
        public Dictionary<AbilityType, float> GetCurStats => _curStatsDict;

        private bool _canJump;
        private bool _canDoubleJump;
        private bool _canDoubleShoot;

        public bool CanJump => _canJump;
        public bool CanDoubleJump => _canDoubleJump;
        public bool CanDoubleShoot => _canDoubleShoot;

        public float CurHp => _curHp;
        public float CurEnergy => _curEnergy;

        //cur value and maxValue(maxHp\energy)
        public static UnityEvent<float, float> OnHpValueChange = new UnityEvent<float, float>();
        public static UnityEvent<float, float> OnEnergyValueChange = new UnityEvent<float, float>();

        private void OnEnable()
        {
            SkillTreeEvents.OnAbilityLearnt.AddListener(OnAbilityLearnt);
            SkillTreeEvents.OnAbilityUnlearnt.AddListener(OnAbilityUnlearn);
            SkillTreeEvents.OnAbilityFullUnlearn.AddListener(OnAbilityFullUnlearn);
        }

        private void OnDisable()
        {
            SkillTreeEvents.OnAbilityLearnt.RemoveListener(OnAbilityLearnt);
            SkillTreeEvents.OnAbilityUnlearnt.RemoveListener(OnAbilityUnlearn);
            SkillTreeEvents.OnAbilityFullUnlearn.RemoveListener(OnAbilityFullUnlearn);
        }
        
        private void Start()
        {
            //Some abilities' functionality isn't implemented, and some are only used for visuals (HP/energy bars)
            //as the task states that it isn't necessary to do so.
            _curStatsDict = new Dictionary<AbilityType, float>
            {
                { AbilityType.MaxHp, _maxHp },
                { AbilityType.HpRegen, _hpRegen },
                { AbilityType.Speed, _moveSpeed },
                { AbilityType.JumpHeight, _jumpHeight },
                { AbilityType.MaxEnergy, _energy },
                { AbilityType.Damage, _damage },
                { AbilityType.AttackSpeed, _attackSpeed }
            };

            _curHp = _curStatsDict[AbilityType.MaxHp];
            _curEnergy = _curStatsDict[AbilityType.MaxEnergy];
            OnHpChanged();
            OnEnergyChanged();
        }

        private void Update()
        {
            RegenStats();
        }

        private void RegenStats()
        {
            if (_curHp < _curStatsDict[AbilityType.MaxHp])
            {
                _curHp += _curStatsDict[AbilityType.HpRegen] * Time.deltaTime;
            }

            if (_curEnergy < _curStatsDict[AbilityType.MaxEnergy])
            {
                _curEnergy += 1 * Time.deltaTime;
            }
        }

        public void ReduceHp(float amount)
        {
            _curHp -= amount;
            OnHpChanged();
        }

        public void ReduceEnergy(float amount)
        {
            _curEnergy -= amount;
            OnEnergyChanged();
        }

        private void OnAbilityLearnt(Ability ability)
        {
            if (_curStatsDict.ContainsKey(ability.AbilityType))
            {
                var value = _curStatsDict[ability.AbilityType];
                _curStatsDict[ability.AbilityType] = ability.OnLevelUp(value);
            }
            else
            {
                switch (ability.AbilityType)
                {
                    case AbilityType.Jump:
                        _canJump = true;
                        break;
                    case AbilityType.DoubleJump:
                        _canDoubleJump = true;
                        break;
                    case AbilityType.DoubleShoot:
                        _canDoubleShoot = true;
                        break;
                }

                ability.OnLevelUp(0f);
            }
            
            _curHp = _curStatsDict[AbilityType.MaxHp];
            OnHpChanged();
            OnEnergyChanged();
        }
        
        private void OnAbilityUnlearn(Ability ability)
        {
            UnlearnAbility(ability, false);
        }
        
        private void OnAbilityFullUnlearn(Ability ability)
        {
            UnlearnAbility(ability, true);
        }

        private void UnlearnAbility(Ability ability, bool isFullUnlearn = false)
        {
            if (_curStatsDict.ContainsKey(ability.AbilityType))
            {
                var value = _curStatsDict[ability.AbilityType];
                _curStatsDict[ability.AbilityType] = isFullUnlearn ? ability.ForceFullUnlearn(value) : ability.OnLevelDown(value);
                
            }
            else
            {
                switch (ability.AbilityType)
                {
                    case AbilityType.Jump:
                        _canJump = false;
                        break;
                    case AbilityType.DoubleJump:
                        _canDoubleJump = false;
                        break;
                    case AbilityType.DoubleShoot:
                        _canDoubleShoot = false;
                        break;
                }

                if (isFullUnlearn) ability.ForceFullUnlearn(0f);
                else ability.OnLevelDown(0f); 
            }
            
            _curHp = _maxHp;
            OnHpChanged();
            OnEnergyChanged();
        }
        
        private void OnHpChanged()
        {
            OnHpValueChange.Invoke(_curHp, _curStatsDict[AbilityType.MaxHp]);
        }

        private void OnEnergyChanged()
        {
            OnEnergyValueChange.Invoke(_curEnergy, _curStatsDict[AbilityType.MaxEnergy]);
        }
    }
}